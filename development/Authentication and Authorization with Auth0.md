# Authentication and Authorization

De Haagse Munt uses the Auth0 service for authorization and authentication. Request from the frontend are made with a Authorization header in the http-request (a JWT token).
The backend server validates this JWT and checks the status and its permissions.

Validating the current user is the first thing this App will be doing. Without authentication there is no access to the dashboard.

## @auth0/auth0-spa-js

We use the @auth0/auth0-spa-js package for all auth0 handling.
Starting point: https://auth0.com/docs/quickstart/spa/react/01-login

1. utils/history.js
   created and used to redirect the user programmatically
2. react-auth0-spa.js
   A Provider with all authentication login and status. This is a functional component that uses Context to store values.
   This is a set of custom React hooks that enable you to work with the Auth0 SDK in a more idiomatic way, providing functions that allow the user to log in, log out, and information such as whether the user is logged in.
   Users who are logged in with username/password will be silently reauthenticated automatically when the application reloads. No further action is needed for this type of login.
   This provider is used in the root element at ./index.js
   This means that any components inside this wrapper will be able to access the Auth0 SDK client.
