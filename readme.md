# React Dashboard

## Create React App

### Redux Store

#### Reading list

- https://github.com/draffauf/react-redux-typescript-demo
- https://github.com/marmelab/react-admin/tree/master/packages
- https://medium.com/javascript-in-plain-english/redux-thunk-vs-redux-saga-8c93fc822de
- https://flaviocopes.com/redux-saga/

### API

### Typescript

- https://www.sitepoint.com/react-with-typescript-best-practices/
- https://react-typescript-cheatsheet.netlify.app/
