# [0.5.0](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/compare/v0.4.0...v0.5.0) (2020-09-09)


### Bug Fixes

* **state:** app state ([f3687d2](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/f3687d2ed5d30394ec70bcf0c144556744796ef5))


### Features

* **core:** add sentfy integration ([828c338](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/828c3383bde24e7910963fdc1bdbfee3213c724b))

# [0.4.0](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/compare/v0.3.3...v0.4.0) (2020-09-08)


### Bug Fixes

* **auth:** update login process ([5309df3](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/5309df38a374c32ec9485af72f1cc54ae4daf29a))
* **primarynav:** update nav with tailwind ([e358c77](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/e358c7798e1e75616c5605e67ada9156ca036848))
* **sidebar:** update sidebar structure ([c212719](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/c2127199b7c55e78d899412baf73c671f775cf46))


### Features

* **search:** add search component ([14e886f](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/14e886f12a795dd9a851186be304f67d0093cfee))
* **style:** implement tailwindcss ([33b9366](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/33b93664c6b43793a06782376b1807b48b329fcb))


### Performance Improvements

* **ci:** rename main branch ([a9c84aa](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/a9c84aa6ca28d703467f2120d55c5332697ccb85))
* **ci:** update install ([9621bd0](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/9621bd077c55f68a96b63a58714cf745355d840b))
* **ci:** update release branch ([abad471](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/abad471fc0be78e678f7385b1bafdffd85d1f850))

## [0.3.3](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/compare/v0.3.2...v0.3.3) (2020-07-07)


### Bug Fixes

* **ci:** update commands ([e653bf3](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/e653bf3b699f228626e08ffae9316f3dad3b5f44))
* **ci:** update stages and deploy to server ([82fb445](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/82fb44537f4188490d323ea71ea10862696d3065))

## [0.3.2](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/compare/v0.3.1...v0.3.2) (2020-07-04)


### Bug Fixes

* update build process ([52b89bd](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/52b89bd4d908cecdca77b626187f06ecc7bdc0f3))

## [0.3.1](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/compare/v0.3.0...v0.3.1) (2020-07-04)


### Bug Fixes

* do not skip CI after Release ([c860297](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/c8602975d29a557027cea98792a42babf624d987))

# [0.3.0](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/compare/v0.2.0...v0.3.0) (2020-07-04)


### Bug Fixes

* run yarn install before yarn test ([3f37a32](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/3f37a32bc9b2a7d63f47649de9ba843e9a3f5ea1))
* updat CI settings for release ([f4660e4](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/f4660e4e70276927f92a6738fbfacdd619b8f1e6))
* update CI/CD settings ([5c663a8](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/5c663a85d101a3fabfb5a2e9b2aa0d98082dc4b4))
* update containers ([441037a](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/441037a55f303751d976d2fb617fdb8e755fd9eb))
* update gitlab-ci to use node:buster-slim ([87651f6](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/87651f6894331954accf8e40e47bf4548f83ed1b))
* update release flow ([9e98970](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/9e98970ab8137a5a25aa885201723d45acfcbc54))


### Features

* update CI workflow with build stages ([425bf8f](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/425bf8f35b60af2dbaa84630c1b98294eec0a68b))
* use semantic release on production branch ([d610f9c](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/commit/d610f9ce6e832245d48999810343e40d84dbe5da))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.1.0](https://gitlab.com/urbanlink/de-haagse-munt/dhm_dashboard/compare/v0.2.0...v0.1.0) (2020-06-16)
