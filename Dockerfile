# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:lts AS build

# set working directory
WORKDIR /usr/src/app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /usr/src/app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json yarn.lock /usr/src/app/

RUN \
    yarn config set ignore-engines true \
    && yarn --pure-lockfile \ 
    && yarn cache clean

COPY . . 

RUN yarn run build


# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:alpine 

COPY nginx/nginx.conf /etc/nginx/nginx.conf

COPY --from=build /usr/src/app/build /usr/src/app/public
