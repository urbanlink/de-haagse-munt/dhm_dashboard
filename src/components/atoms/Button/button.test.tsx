import React from 'react';
import { render } from '@testing-library/react';
import Button from './button';

it('renders without crashing', () => {
  const { getByTestId } = render(<Button>Hello</Button>);

  expect(getByTestId('button')).toBeInTheDOM;
});
