import React, { useCallback, useState } from 'react';

import { toCurrency } from '../../../../utils/currency-conversion';

// HELPERS
// Validate for postive float
const isPositiveNumber = (amount: string) => {
  const r = parseFloat(amount.replace(/,/, '.'));
  return !isNaN(r) && r > 0;
};

const addtrailingZeros = (amount: string, separator = ',') => {
  let output;
  let values = amount.split(separator).slice(0, 2);

  if (!values[1]) {
    output = values[0] + ',00';
  } else {
    console.log(values[1].length);
    while (values[1].length < 2) {
      values[1] += '0';
    }
    console.log(values);
    output = values.join(separator);
  }

  return output;
};

// INTERFACES
interface OnValueChange {
  (result: { valid: boolean; err: string | undefined; amount: string }): any;
}

interface Props {
  onValueChange: OnValueChange;
  value?: string;
  placeholder?: string;
  inputProps: {
    max: number;
  };
}

// COMPONENT
const CurrencyInput: React.FC<Props> = ({ onValueChange, value, inputProps }) => {
  // const inputRef = useRef();
  const { max } = inputProps || 0;

  const [pristine, setPristine] = useState(true);
  // const [valid, setValid] = useState(false);
  const [err, setError] = useState<String | undefined>(undefined);

  //
  const parseAmount = useCallback(
    (amount) => {
      // Convert input to currency string
      const valueAsCurrency = toCurrency(amount, ',');

      if (!isPositiveNumber(valueAsCurrency)) {
        // setValid(false);
        setError('Not a positive number');
        onValueChange({
          valid: false,
          err: 'Not a positive number',
          amount: valueAsCurrency,
        });
        return;
      }

      // Make sure requested amount is not more than current balance
      if (parseFloat(valueAsCurrency.replace(',', '.')) * 100 > max) {
        // setValid(false);
        setError(`Je kunt maximaal ${(max / 100).toFixed(2)} overmaken.`);
        onValueChange({
          valid: false,
          err: 'Max received',
          amount: valueAsCurrency,
        });
        return;
      }

      // Set value
      setError(undefined);
      // setValid(true);
      onValueChange({
        valid: true,
        err: undefined,
        amount: valueAsCurrency,
      });
    },
    [max, onValueChange],
  );

  const handleChange = useCallback(
    (e) => {
      if (pristine) setPristine(false);
      parseAmount(e.target.value);
    },
    [parseAmount, pristine],
  );

  const handleBlur = useCallback(
    (e) => {
      const value = addtrailingZeros(e.target.value);
      parseAmount(value);
    },
    [parseAmount],
  );
  return (
    <div>
      <div>Bedrag</div>
      <input
        type="text"
        pattern="\d*"
        {...inputProps}
        value={value}
        onChange={handleChange}
        onBlur={handleBlur}
        // pristine={pristine}
        // valid={valid}
        // inputRef={inputRef}
      />
      <div>{err}</div>
    </div>
  );
};

CurrencyInput.defaultProps = {
  value: '',
  inputProps: { max: 0 },
};

export default CurrencyInput;
