import React from 'react';

// interface OnValueChange {
//   (result: { valid: boolean; err: string | undefined; amount: string }): any;
// }

// interface Props {
//   onValueChange: OnValueChange;
//   value?: string;
//   placeholder?: string;
//   inputProps: {
//     max: number;
//   };
// }

const InputTextarea: React.FC<any> = (onValueChange, { children }) => {
  return <textarea onChange={onValueChange}>{children}</textarea>;
};

export default InputTextarea;
