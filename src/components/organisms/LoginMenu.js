import React, { Component } from 'react';
import { getUserInfo } from '../../utils/user.service';
import { isBrowser, isAuthenticated } from '../../utils/auth.service';

class LoginMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: null,
      authenticated: null,
    };
  }

  componentDidMount() {
    this.setState({
      userInfo: getUserInfo(),
      authenticated: isAuthenticated(),
    });
  }

  render() {
    console.log(this.state.userInfo);
    // Conditionalky return component
    if (!this.state.userInfo || !this.state.authenticated) {
      return <LoginBtn to="/login">Inloggen</LoginBtn>;
    } else if (isBrowser) {
      return (
        <Link to="/settings">
          <Avatar alt="avatar" src={this.state.userInfo.picture} />
        </Link>
      );
    }

    return null;
  }
}

export default LoginMenu;
