import React from 'react';

class AccountMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenu: false,
    };
  }

  componentDidMount() {}

  showMenu = (evt) => {
    evt.preventDefault();

    this.setState({ showMenu: true }, () => {
      document.addEventListener('click', this.closeMenu);
    });
  };

  closeMenu = () => {
    this.setState({ showMenu: false }, () => {
      document.removeEventListener('click', this.closeMenu);
    });
  };

  render() {
    return (
      <DropdownContainer>
        <ToggleButton onClick={this.showMenu}>
          <Dot />
          <Dot />
          <Dot />
        </ToggleButton>

        {this.state.showMenu ? (
          <Menu>
            <MenuItem>
              <NavLink to="/account">Account</NavLink>
            </MenuItem>
            <MenuItem>
              <NavLink to="/voucher">Voucher inwisselen</NavLink>
            </MenuItem>
            <MenuItem>
              <NavLink to="/inleg">Nieuwe inleg</NavLink>
            </MenuItem>
            <MenuItem>
              <NavLink to="/contacten">Adresboek</NavLink>
            </MenuItem>
            <MenuItem>
              <NavLink to="/berichten">Berichten</NavLink>
            </MenuItem>
            <MenuItem>
              <NavLink to="/service">Service</NavLink>
            </MenuItem>
          </Menu>
        ) : null}
      </DropdownContainer>
    );
  }
}

export default AccountMenu;
