/**
 *
 * Transaction model definition
 *
 */
export interface TransactionModel {
  id: number;
  amount: string;
  to_id: number;
  to_nickname: string;
  to_picture: string;
  from_nickname: string;
  from_picture: string;
  amountmeta: number[];
  type: string;
  description: string;
  created_at: string;
}
