export interface IMessage {
  id: number;
  read_status: boolean;
  subject: string;
  createdAt: string;
  body: string;
}
