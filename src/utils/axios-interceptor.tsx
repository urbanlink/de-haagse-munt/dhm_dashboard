import Axios from 'axios';

export const setAxiosTokenInterceptor = async (accessToken: string): Promise<void> => {
  Axios.interceptors.request.use(async (config) => {
    const requestConfig = config;
    if (accessToken) {
      requestConfig.headers.common.Authorization = `Bearer ${accessToken}`;
    }
    return requestConfig;
  });
};
