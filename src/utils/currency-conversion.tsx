// Helper function to remove alle non-numeric characters
const getDigitsFromValue = (value = ''): string =>
  value.replace(/(-(?!\d))|[^0-9|-]/g, '') || '';

// Remove 0 character from the beginning of the value
const removeLeadingZeros = (number: string): string =>
  number.replace(/^0+([0-9]+)/, '$1');

// Convert a given string into a currency  nnn[. ,]nn
export const toCurrency = (value: string, separator = ','): string => {
  // split into main and fraction
  // only get 2 first parts (main,fraction)
  let values: Array<string> = value.split(separator).slice(0, 2);

  // clean main
  let main: string = values[0];
  let fraction: string = values[1];
  main = getDigitsFromValue(main);
  main = removeLeadingZeros(main);
  // make absolute
  if (main) {
    main = Math.abs(parseInt(main)).toString();
  }
  let result: string = main;

  // handle fraction if needed
  if (fraction !== undefined) {
    fraction = getDigitsFromValue(fraction);
    if (fraction.length > 2) {
      fraction = fraction.substr(0, 2);
    }
    result = main + separator + fraction;
  }

  return result;
};
