// Custom Currency functions

// Convert input to currency string
// Input: value in cents
// Output: n.nnn,nn HM

interface Props {
  value: number;
  options?: any;
}

export const DisplayCurrency = ({ value }: Props): string => {
  return (
    (value / 100).toLocaleString('nl-NL', {
      maximumFractionDigits: 2,
      minimumFractionDigits: 2,
    }) + ' HM'
  );
};
