import { useAuth0 } from '@auth0/auth0-react';
import { useState, useEffect } from 'react';

interface IOptions {
  audience?: string;
  scope?: string;
  headers?: any;
}
export const useApi = (url: string, options: IOptions) => {
  const { getAccessTokenSilently } = useAuth0();
  const [state, setState] = useState({
    error: null,
    loading: true,
    data: null,
  });

  const [refreshIndex, setRefreshIndex] = useState(0);

  useEffect(() => {
    console.log('call api');
    (async () => {
      try {
        const { scope, ...fetchOptions } = options;
        const audience = process.env.REACT_APP_DHM_AUTH0_AUDIENCE;
        const accessToken = await getAccessTokenSilently({ audience, scope });

        const res = await fetch(`${process.env.REACT_APP_API_URL}${url}`, {
          ...fetchOptions,
          headers: {
            ...fetchOptions.headers,
            Authorization: `Bearer ${accessToken}`,
          },
        });
        setState({
          ...state,
          data: await res.json(),
          error: null,
          loading: false,
        });
      } catch (error) {
        setState({
          ...state,
          error,
          loading: false,
        });
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [refreshIndex]);

  return {
    ...state,
    refresh: () => setRefreshIndex(refreshIndex + 1),
  };
};
