// https://github.com/auth0/auth0-react/blob/master/EXAMPLES.md#4-create-a-useapi-hook-for-accessing-protected-apis-with-an-access-token

import React, { createContext, useContext } from 'react';
import { PropTypes } from 'prop-types';
import Axios from 'axios';
import { useAuth0 } from './auth0Context';

export const ApiContext = createContext();
export const useApi = () => useContext(ApiContext);

export const ApiProvider = ({ children }) => {
  const { getTokenSilently } = useAuth0();

  const searchByUsername = async (p) => {
    const url = `${process.env.REACT_APP_DHM_API_URL}/user/search?nickname=${p.nickname}`;
    const token = await getTokenSilently();
    try {
      const fetchResult = await Axios({
        method: 'get',
        url,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      return fetchResult.data;
    } catch (err) {
      console.warn(err);
      return err;
    }
  };

  const startTransfer = async (data) => {
    const url = `${process.env.REACT_APP_DHM_API_URL}/transfer`;
    const token = await getTokenSilently();
    try {
      const r = await Axios({
        method: 'post',
        url,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      return r.data;
    } catch (err) {
      console.warn(err);
      return err;
    }
  };

  const getMessages = async () => {
    const url = `${process.env.REACT_APP_DHM_API_URL}/message`;
    const token = await getTokenSilently();
    try {
      const r = await Axios({
        method: 'get',
        url,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      return r.data;
    } catch (err) {
      console.warn(err);
    }
  };

  const updateMessage = async (data) => {
    const url = `${process.env.REACT_APP_DHM_API_URL}/message/${data.id}`;
    const token = await getTokenSilently();
    try {
      const r = await Axios({
        method: 'put',
        url,
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data,
      });
      return r.data;
    } catch (err) {
      console.warn(err);
    }
  };

  const createPayment = async (data) => {
    const url = `${process.env.REACT_APP_DHM_API_URL}/payment`;
    const token = await getTokenSilently();
    try {
      const r = await Axios({
        method: 'post',
        url,
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data,
      });
      return r.data;
    } catch (err) {
      console.warn(err);
    }
  };

  const redeemVoucher = async (data) => {
    const url = `${process.env.REACT_APP_DHM_API_URL}/voucher/redeem`;
    const token = await getTokenSilently();
    try {
      const r = await Axios({
        method: 'post',
        url,
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data,
      });
      return r.data;
    } catch (err) {
      console.warn(err);
    }
  };

  const config = {
    searchByUsername: (...p) => searchByUsername(...p),
    startTransfer: (...p) => startTransfer(...p),
    getMessages: (...p) => getMessages(...p),
    updateMessage: (...p) => updateMessage(...p),
    createPayment: (...p) => createPayment(...p),
    redeemVoucher: (...p) => redeemVoucher(...p),
  };

  return <ApiContext.Provider value={config}>{children}</ApiContext.Provider>;
};

ApiProvider.propTypes = {
  children: PropTypes.object.isRequired,
};
