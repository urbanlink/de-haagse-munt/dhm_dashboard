import axios from 'axios';

export default {
  setupInterceptors: (history: any) => {
    // Add a response interceptor
    axios.interceptors.response.use(
      (response) => {
        return response;
      },
      function (error) {
        console.warn('axios interceptor', { error });
        if (error.message === 'Network Error') {
          console.log('The bank API is offline! ');
          history.push('/offline');
        } else if (error.response.status === 401) {
          history.push('/login');
        } else if (error.response.status === 404) {
          history.push('/not-found');
        } else {
        }

        return Promise.reject(error);
      }
    );
  },
};
