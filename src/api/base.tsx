import axios, { Method } from 'axios';
// import { getTokenSilently } from "../react-auth0-spa";

const BASE_URL = 'http://localhost:4000';

const CallAPI = async (
  endpoint = '',
  method: Method = 'get',
  data = {}
): Promise<any> => {
  // const { loading, getTokenSilently } = useAuth0();
  // console.log(loading);
  console.log('callapi');
  // const token = await getTokenSilently();
  // console.log('token', token);

  // const endpoint = "";
  // const method = "get";
  // const data = {};

  return await axios({
    method,
    // headers: {
    //   Authorization: `Bearer ${token}`,
    // },
    url: `${BASE_URL}${endpoint}`,
    data,
  });
};

export default CallAPI;
