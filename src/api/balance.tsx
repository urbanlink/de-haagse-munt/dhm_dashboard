import CallAPI from './base';

export interface iCurrentBalance {
  amount: number;
  amountmeta: number[];
}

// Get user's current balance
export const getCurrentBalance = async (): Promise<any> => {
  console.log('getCurrentBal;ac');
  return await CallAPI(`/balance/current`, 'get', {});
};
