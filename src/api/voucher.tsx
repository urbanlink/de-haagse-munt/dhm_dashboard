import CallAPI from './base';

// Get user's messages
export const getMyMessages = (params = '') =>
  CallAPI(`/messages/my/${params}`, 'get', {});

// Get single message
export const getSingleMessage = (id: number) =>
  CallAPI(`/messages/${id}`, 'get', {});

// Toggle message read
export const toggleMessageRead = (id: number) =>
  CallAPI(`/messages/${id}/toggle-read`, 'put', {});

// Remove message
export const removeMessage = (id: number) =>
  CallAPI(`/messages/${id}`, 'delete', {});
