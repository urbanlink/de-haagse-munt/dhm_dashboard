import CallAPI from './base';

// Get user's transfers
export const getMyTransfers = (params = '') =>
  CallAPI(`/transfers/my/${params}`, 'get', {});

// Get single transfer
export const getSingleTransfer = (id: number) =>
  CallAPI(`/transfers/${id}`, 'get', {});

// Create a new transfer
export const createTransfer = (data: any) =>
  CallAPI(`/transfers`, 'post', data);
