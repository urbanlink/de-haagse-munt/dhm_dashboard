import React from 'react';
import moment from 'moment';

import { IMessage } from '../../../types/Message';

interface MessagesListItemProps {
  message: IMessage;
  onMessageClicked: (id: number) => void;
  selected: boolean;
}

export const MessagesListItem: React.FC<MessagesListItemProps> = ({ message, onMessageClicked }) => {
  // useEffect(() => {
  //   console.log('stgate change messagelist', selected);
  // });

  /*
  body: "
createdAt: "2020-01-06T09:00:01.007Z"
delete_status: false
emailed_status: true
id: 1
level: "info"
read_status: true
subject: "Welkom!"
type: "system"
*/
  const parseDate = (d: string) => {
    return moment(d).format('DD MMMM YYYY - H:mm');
  };

  return (
    <div
      onClick={() => {
        onMessageClicked(message.id);
      }}
      // selected={selected}
    >
      <div
      // read={message.read_status}
      />
      <div>{message.subject}</div>
      <div>{parseDate(message.createdAt)}</div>
    </div>
  );
};
