import React from 'react';
import moment from 'moment';
import { IMessage } from '../../../types/Message';

interface MessagesDetailsProps {
  message: IMessage;
  onDelete: (id: number) => void;
}

const MessagesDetails: React.FC<MessagesDetailsProps> = ({ message, onDelete }) => {
  if (!message) return null;

  return (
    <div className="message-content">
      <header className="message-content__header">
        <div className="message-content__subject">{message.subject}</div>
        <a onClick={() => onDelete(message.id)}>
          <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24">
            <path d="M3 6v18h18v-18h-18zm5 14c0 .552-.448 1-1 1s-1-.448-1-1v-10c0-.552.448-1 1-1s1 .448 1 1v10zm5 0c0 .552-.448 1-1 1s-1-.448-1-1v-10c0-.552.448-1 1-1s1 .448 1 1v10zm5 0c0 .552-.448 1-1 1s-1-.448-1-1v-10c0-.552.448-1 1-1s1 .448 1 1v10zm4-18v2h-20v-2h5.711c.9 0 1.631-1.099 1.631-2h5.315c0 .901.73 2 1.631 2h5.712z" />
          </svg>
        </a>
        <div className="message-content__time">{moment(message.createdAt).format('DD MMMM YYYY - H:mm')}</div>
      </header>

      <div className="message-content__body">
        <div dangerouslySetInnerHTML={{ __html: message.body }}></div>
      </div>
    </div>
  );
};

export default MessagesDetails;
