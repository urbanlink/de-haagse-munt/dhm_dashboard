import React from 'react';

import { MessagesListItem } from './../MessagesListItem';
import { IMessage } from '../../../types/Message';

interface MessagesListProps {
  messages: IMessage[];
  onMessageSelected: (id: number) => void;
  selectedMessageId: number;
}

export const MessagesList: React.FC<MessagesListProps> = ({ messages, onMessageSelected, selectedMessageId }) => {
  // useEffect(() => {
  //   console.log('stgate change messagelist', selectedMessageId);
  // });

  return (
    <div>
      {messages.map((message) => (
        <MessagesListItem
          key={message.id}
          message={message}
          onMessageClicked={(id) => {
            onMessageSelected(id);
          }}
          selected={selectedMessageId === message.id}
        />
      ))}
    </div>
  );
};
