import React from 'react';
import moment from 'moment';

import { DisplayCurrency } from '../../../utils/display-currency';
// import { ArrowLeftRight } from '../../../atoms/icons/ArrowLeftRight';
import { TransactionModel } from '../../../types/Transaction';

interface Props {
  transaction: TransactionModel;
}

export const TransactionsListItem: React.FC<Props> = ({ transaction }) => {
  const state = { account: { id: null } }; //useTrackedState();
  const { account } = state;
  if (!account) return null;

  const received = (): boolean => transaction.to_id === account.id;

  // const sentOrReceived = () => (received() === true ? 'bij' : 'af');

  const getTotal = () => DisplayCurrency({ value: transaction.amountmeta.reduce((a, b) => a + b, 0) });

  if (!transaction) return null;
  if (transaction.type === 'transfer') {
    transaction.type = 'overschrijving';
  }

  const TransactionType = () => {
    return null;
    // (
    //   // <Type>
    //   //   <ArrowLeftRight />
    //   // </Type>
    // );
  };

  return (
    <div data-received={received()}>
      <div>{moment(transaction.created_at).format('DD-MM-YYYY')}</div>
      <div>
        <div data-received={received()}>{received() === true ? 'bij' : 'af'}</div>
        <div>{DisplayCurrency({ value: parseFloat(transaction.amount) })}</div>
        {/* <AmountMeta>{transaction.amount_meta}</AmountMeta> */}

        <TransactionType />

        <div>
          {received() && transaction.type === 'overschrijving' && (
            <div>
              <img src={transaction.from_picture} alt="from" />
              {transaction.from_nickname}
            </div>
          )}
          {!received() && transaction.type === 'overschrijving' && (
            <div>
              <img src={transaction.to_picture} alt="to" />
              {transaction.to_nickname}
            </div>
          )}
        </div>
        <div>{getTotal()}</div>
      </div>
      <div>{transaction.description}</div>
    </div>
  );
};
