import React from 'react';

import { TransactionModel } from '../../types/Transaction';
import { TransactionsListItem } from './TransactionListItem/transactionsListItem';

interface Props {
  transactions: TransactionModel[];
}

const TransactionsList: React.FC<Props> = ({ transactions }) => {
  if (!transactions) return null;

  return (
    <div>
      {transactions.length === 0 && <p>Nog geen transacties bekend.</p>}

      {transactions.length > 0 && (
        <ul>
          {transactions.map((t: TransactionModel) => (
            <TransactionsListItem key={t.id} transaction={t} />
          ))}
        </ul>
      )}
    </div>
  );
};

export default TransactionsList;
