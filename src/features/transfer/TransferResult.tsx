import React from 'react';

import Button from '../../components/atoms/Button';

export default class TransferResult extends React.Component {
  render() {
    // const {result} = this.props;

    return (
      <div>
        <h1>Overschrijven</h1>
        <h2>Resultaat van de overschrijving</h2>

        <div>
          <p>De transactie is geslaagd!</p>
        </div>

        <div>
          <div>
            <Button to={'/'}>Overzicht &rarr;</Button>
          </div>
        </div>
      </div>
    );
  }
}
