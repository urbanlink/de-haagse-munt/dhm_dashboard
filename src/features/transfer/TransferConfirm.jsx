import React from 'react';

import { DisplayCurrency } from '../../utils/currency';
import { PropTypes } from 'prop-types';

const TransferConfirm = ({ values, onBack, onConfirm }) => {
  return (
    <Wrapper>
      <Title>Overschrijven</Title>
      <Subtitle>Weet je zeker dat je deze overschrijving wilt uitvoeren?</Subtitle>

      <ReceiptWrapper>
        <table>
          <tbody>
            {values.recipient && (
              <tr>
                <TD>
                  <Label>Ontvanger</Label>
                </TD>
                <TD>
                  <div style={tw`display: flex;content-center flex-wrap`}>
                    <Avatar>
                      <AvatarImg src={values.recipient.picture} />
                    </Avatar>
                    {values.recipient.nickname}
                  </div>
                </TD>
              </tr>
            )}
            <tr>
              <TD>
                <Label>Bedrag</Label>
              </TD>
              <TD>HM {DisplayCurrency(values.amount)}</TD>
            </tr>
            {description && (
              <tr>
                <TD>
                  <Label>Beschrijving</Label>
                </TD>
                <TD>{values.description}</TD>
              </tr>
            )}
          </tbody>
        </table>
      </ReceiptWrapper>

      <ActionsWrapper>
        <Action>
          <BackLink onClick={() => onBack}>Terug</BackLink>
        </Action>
        <Action>
          <Button onClick={() => onConfirm}>Overschrijven &rarr;</Button>
        </Action>
      </ActionsWrapper>
    </Wrapper>
  );
};

// Prop validation
TransferConfirm.propTypes = {
  onBack: PropTypes.func,
  onConfirm: PropTypes.func,
  values: PropTypes.shape({
    amount: PropTypes.number.isRequired,
    description: PropTypes.string,
    recipient: PropTypes.shape({
      nickname: PropTypes.string.isRequired,
      picture: PropTypes.string.isRequired,
    }),
  }),
};

export default TransferConfirm;
