import React from 'react';
import ReactModal from 'react-modal';
import Button from '../../../components/atoms/Button';

ReactModal.setAppElement('#modal');

// interface Props {
//   showModal: boolean;
//   values: object;
//   // setShowModal: func,
//   // confirmTransaction: func,
// }

export const TransferConfirmModal: React.FC<any> = ({ showModal, values, setShowModal, confirmTransaction }) => {
  return (
    <ReactModal isOpen={showModal} contentLabel="Minimal Modal Example">
      <div>
        <h1>Bevestig de volgende overschrijving: </h1>
        <div>
          <table>
            <tbody>
              <tr>
                <td>Bedrag:</td>
                <td>{values.amount} HM</td>
              </tr>
              <tr>
                <td>Aan:</td>
                <td>
                  <div>
                    <img src={values.recipient.picture} />
                    <div>{values.recipient.nickname}</div>
                  </div>
                </td>
              </tr>
              <tr>
                <td>Beschrijving:</td>
                <td>{values.description && <span>&quot;{values.description}&quot;</span>}</td>
              </tr>
            </tbody>
          </table>
        </div>

        <Button onClick={confirmTransaction}>Overschrijving bevestigen</Button>
        <br />
        <Button onClick={() => setShowModal(false)}>Annuleren</Button>
      </div>
    </ReactModal>
  );
};
