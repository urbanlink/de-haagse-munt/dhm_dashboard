import React from 'react';
import ReactModal from 'react-modal';
// import { Link } from 'react-router-dom';
import Button from '../../../components/atoms/Button';

ReactModal.setAppElement('#modal');

interface Props {
  showModal: boolean;
}

export const TransferResult: React.FC<Props> = ({ showModal }) => {
  return (
    <ReactModal isOpen={showModal} contentLabel="Minimal Modal Example">
      <div>
        <h1>Overschrijven</h1>
        <h2>Resultaat van de overschrijving</h2>

        <div>
          <p>De transactie is geslaagd!</p>
        </div>

        <div>
          <div>
            <Button to={'/'}>Overzicht &rarr;</Button>
          </div>
        </div>
      </div>
    </ReactModal>
  );
};
