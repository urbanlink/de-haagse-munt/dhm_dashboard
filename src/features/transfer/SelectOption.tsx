import React from 'react';
import { components } from 'react-select';

// Select Component for option list
export const SelectOption: React.FC<any> = (props) => {
  return (
    <components.Option {...props}>
      <div>
        <div>
          <img src={props.data.picture} />
        </div>
        <div>{props.data.nickname}</div>
      </div>
    </components.Option>
  );
};
