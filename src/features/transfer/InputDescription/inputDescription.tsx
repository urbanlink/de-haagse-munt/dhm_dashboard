import React, { useState } from 'react';

const InputDescription: React.FC<any> = () => {
  const [error] = useState<string | undefined>();
  const [description] = useState('');

  const maxLength = 150;

  //   const validateChange = (e: React.FormEvent<HTMLInputElement>) => {
  //     let d: string = e.currentTarget.value;
  //     if (d.length > maxLength) {
  //       d = d.substring(0, maxLength);
  //     }
  //     setDescription(d);
  //     handleChange({ description: d });
  //     if (false) {
  //       setError('There was an error');
  //     }
  //   };

  return (
    <div>
      <div>Beschrijving</div>
      {/* <Textarea
        name="description"
        rows="5"
        placeholder="Optionele beschrijving"
        value={description}
        onChange={validateChange}
      /> */}
      <div>{maxLength - description.length} tekens beschikbaar</div>

      {error}
    </div>
  );
};

export default InputDescription;
