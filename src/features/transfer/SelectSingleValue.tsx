import React from 'react';
import { components } from 'react-select';

export const SingleValue: React.FC<any> = (props) => {
  return (
    <components.SingleValue {...props}>
      <div className="selectWrapper">
        <div>
          <img src={props.data.picture} />
        </div>
        <div>{props.data.nickname}</div>
      </div>
    </components.SingleValue>
  );
};
