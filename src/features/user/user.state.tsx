import { createState, useState } from '@hookstate/core';
import { IUser } from './user.interface';

const userState = createState<IUser | null>(null);

// Export the usState function for the userstate
export const useUserState = () => useState(userState);
