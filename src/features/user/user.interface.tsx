export interface IUser {
  account_plan: string;
  account_plan_end: null;
  admin: boolean;
  auth0: string;
  bio: string;
  createdAt: Date;
  firstname: string;
  id: number;
  last_email_notification: Date;
  lastname: null;
  nickname: string;
  notification_schedule: number;
  picture: string;
  signed_up: Date;
  slug: string;
}
