export interface IBalance {
  amount: string;
  amountMeta: number[];
}
