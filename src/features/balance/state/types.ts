// Describing the shape of the balance's slice of state
export interface IBalance {
  balance?: number;
  balanceMeta?: number[];
}

export interface IBalanceState {
  balance?: number;
  balanceMeta?: number[];
  isFetching: Boolean;
  errorMessage?: string;
}

export enum BalanceActionTypes {
  FETCH_BALANCE_START = 'FETCH_BALANCE_START',
  FETCH_BALANCE_SUCCESS = 'FETCH_BALANCE_SUCCESS',
  FETCH_BALANCE_ERROR = 'FETCH_BALANCE_ERROR',
}

export interface IFetchBalanceStartAction {
  type: BalanceActionTypes.FETCH_BALANCE_START;
  isFetching: true;
}
export interface IFetchBalanceSuccessAction {
  type: BalanceActionTypes.FETCH_BALANCE_SUCCESS;
  balance: IBalance;
  isFetching: false;
}
export interface IFetchBalanceErrorAction {
  type: BalanceActionTypes.FETCH_BALANCE_ERROR;
  isFetching: false;
  errorMessage: string;
}

type BalanceActions = IFetchBalanceStartAction | IFetchBalanceSuccessAction | IFetchBalanceErrorAction;

export default BalanceActions;
