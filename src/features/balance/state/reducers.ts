import BalanceActions, { BalanceActionTypes, IBalanceState } from './types';

const INITIAL_STATE: IBalanceState = {
  balance: undefined,
  balanceMeta: undefined,
  isFetching: false,
  errorMessage: undefined,
};

export const balanceReducer = (state = INITIAL_STATE, action: BalanceActions): IBalanceState => {
  switch (action.type) {
    case BalanceActionTypes.FETCH_BALANCE_START: {
      return {
        ...state,
        isFetching: action.isFetching,
      };
    }
    case BalanceActionTypes.FETCH_BALANCE_SUCCESS: {
      return {
        ...state,
        balance: Number(action.balance),
        isFetching: action.isFetching,
      };
    }
    case BalanceActionTypes.FETCH_BALANCE_ERROR: {
      return {
        ...state,
        isFetching: action.isFetching,
        errorMessage: action.errorMessage,
      };
    }
    default:
      return state;
  }
};
