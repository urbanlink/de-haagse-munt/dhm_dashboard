// import { Action } from 'redux';
// import { ThunkAction } from 'redux-thunk';

import { IBalance, BalanceActionTypes } from './types';
// import { AppState } from '../../../redux/store';

//  Action creators, to fire an action that the reducer handles
export const fetchBalanceStarted = () => ({
  type: BalanceActionTypes.FETCH_BALANCE_START,
});

export const fetchBalanceSuccess = (balance: IBalance) => ({
  type: BalanceActionTypes.FETCH_BALANCE_SUCCESS,
  payload: balance,
});

export const fetchBalanceError = (errorMessage: string) => ({
  type: BalanceActionTypes.FETCH_BALANCE_ERROR,
  payload: errorMessage,
});

// Thunk action creator
// export const fetchBalance = (): ThunkAction<void, AppState, null, Action<string>> => async (dispatch) => {
//   dispatch(fetchBalanceStarted());

//   const asyncResp = await ExampleApi();

//   dispatch(fetchBalanceSuccess(asyncResp));
// };

// Temp API
// const ExampleApi = async () => {
//   return Promise.resolve({ balance: 12, balanceMeta: [100, 0, 0] });
// };
