import { IBalance } from './types';
import { BalanceActionTypes } from './types';

export interface IFetchBalanceStartAction {
  type: BalanceActionTypes.FETCH_BALANCE_START;
  isFetching: true;
}
export interface IFetchBalanceSuccessAction {
  type: BalanceActionTypes.FETCH_BALANCE_SUCCESS;
  balance: IBalance;
  isFetching: false;
}
export interface IFetchBalanceFailureAction {
  type: BalanceActionTypes.FETCH_BALANCE_ERROR;
  isFetching: false;
}
