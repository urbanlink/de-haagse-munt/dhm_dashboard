import React from 'react';
import { fetchBalance } from '../actions/balance';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class BalanceBox extends React.Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchBalance);
  }

  render() {
    console.log(this.props);
    const { balance } = this.props;
    return <div>Current balance: {balance}</div>;
  }
}

BalanceBox.defaultProps = {
  balance: null,
};

BalanceBox.propTypes = {
  dispatch: PropTypes.func.isRequired,
  balance: PropTypes.number,
  error: PropTypes.string,
  loading: PropTypes.bool,
};

const mapStateToProps = (state) => {
  const { balance, error, loading } = state.balance;
  return { balance, error, loading };
};

export default connect(mapStateToProps)(BalanceBox);
