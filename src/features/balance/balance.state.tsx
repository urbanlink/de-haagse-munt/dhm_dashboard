import { createState, useState } from '@hookstate/core';
import { IBalance } from './state/types';

const balanceState = createState<IBalance | null>(null);

// Export the usState function for the userstate
export const useBalanceState = () => useState(balanceState);
