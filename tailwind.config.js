module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
  },
  purge: ['./src/**/*.html', './src/**/*.tsx'],
  theme: {
    fontFamily: {
      sans: ['Titillium web', 'Arial', 'sans-serif']
    },
    extend: {},
  },
  variants: {},
  plugins: [],
};
